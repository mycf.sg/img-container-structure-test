FROM public.ecr.aws/docker/library/alpine:3.17 AS base
RUN apk --no-cache update \
    && apk --no-cache upgrade \
    && apk add --no-cache git jq bash curl make

WORKDIR /usr/local/bin
RUN curl -LO https://storage.googleapis.com/container-structure-test/latest/container-structure-test-linux-amd64 \
    && mv ./container-structure-test-linux-amd64 ./container-structure-test \
    && chmod +x ./container-structure-test \
    && ./container-structure-test version > /version

WORKDIR /

ENTRYPOINT ["/bin/bash", "-l"]

FROM base AS dind
RUN apk add --no-cache docker python3 py-pip gcc python3-dev libc-dev libffi-dev openssl-dev \
    && pip install --upgrade pip \
    && pip install --upgrade docker-compose \
    && apk del --no-cache gcc python3-dev libc-dev libffi-dev openssl-dev

VOLUME [ "/var/run/docker.sock" ]

ARG DOCKER_HOST="tcp://localhost:2375"
ENV DOCKER_HOST="${DOCKER_HOST}"
