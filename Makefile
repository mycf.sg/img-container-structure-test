IMAGE_PATH=mycfsg/container-structure-tests
ECR_IMAGE_PATH=public.ecr.aws/x2l2g4d3/container-structure-tests

build:
	@$(MAKE) build_base
	@$(MAKE) build_dind

test: build
	@$(MAKE) test_base
	@$(MAKE) test_dind

publish: test
	@$(MAKE) publish_base
	@$(MAKE) publish_dind

build_base:
	@docker build --target=base -f ./Dockerfile -t $(IMAGE_PATH):latest .

test_base: build_base
	@container-structure-test test --image $(IMAGE_PATH):latest --config ./Dockerfile.base.yaml

publish_base: test_base
	@docker run --entrypoint=cat $(IMAGE_PATH):latest /version > ./.version
	@docker tag $(IMAGE_PATH):latest $(IMAGE_PATH):$$(cat ./.version)
	# @docker push $(IMAGE_PATH):latest
	# @docker push $(IMAGE_PATH):$$(cat ./.version)
	@docker tag $(IMAGE_PATH):latest $(ECR_IMAGE_PATH):$$(cat ./.version)
	@docker push $(ECR_IMAGE_PATH):latest
	@docker push $(ECR_IMAGE_PATH):$$(cat ./.version)

build_dind:
	@docker build --target=dind -f ./Dockerfile -t $(IMAGE_PATH)-dind:latest .

test_dind: build_dind
	@container-structure-test test --image $(IMAGE_PATH)-dind:latest --config ./Dockerfile.base.yaml
	@container-structure-test test --image $(IMAGE_PATH)-dind:latest --config ./Dockerfile.dind.yaml

publish_dind: test_dind
	@docker run --entrypoint=cat $(IMAGE_PATH)-dind:latest /version > ./.version
	@docker tag $(IMAGE_PATH)-dind:latest $(IMAGE_PATH)-dind:$$(cat ./.version)
	# @docker push $(IMAGE_PATH)-dind:latest
	# @docker push $(IMAGE_PATH)-dind:$$(cat ./.version)
	@docker tag $(IMAGE_PATH)-dind:latest $(ECR_IMAGE_PATH)-dind:$$(cat ./.version)
	@docker push $(ECR_IMAGE_PATH)-dind:latest
	@docker push $(ECR_IMAGE_PATH)-dind:$$(cat ./.version)