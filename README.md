# Container Structure Test
This repository contains an image meant for CI pipeline use that contains the `container-structure-test` tool by Google.

See [`container-structure-test`] for information on the tool.

# Usage

## DinD Version
The image is available at [mycfsg/container-structure-test-dind](https://gallery.ecr.aws/mycfsg/container-structure-tests-dind).

# License
This project is licensed under the MIT license. See [the LICENSE file](./LICENSE) for the full text.
